libhtml-formhandler-perl (0.40068-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since buster
    * Build-Depends-Indep: Drop versioned constraint on libjson-maybexs-perl,
      libmoose-perl, libscalar-list-utils-perl and perl.
    * libhtml-formhandler-perl: Drop versioned constraint on
      libjson-maybexs-perl, libmoose-perl, libscalar-list-utils-perl and perl
      in Depends.

  [ gregor herrmann ]
  * Add (build) dependency on libhttp-date-perl.
    Thanks to autopkgtests.
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Update alternative build dependencies.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Mar 2022 17:32:07 +0100

libhtml-formhandler-perl (0.40068-1) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Drop debian/tests/pkg-perl/smoke-tests, handled by pkg-perl-
    autopkgtest now.

  [ Damyan Ivanov ]
  * New upstream version 0.40068
  * update years of upstream copyright
  * add libcrypt-{blowfish,cbc}-perl to (build) dependencies
  * patch the WHATIS entries of two manpages

 -- Damyan Ivanov <dmn@debian.org>  Sat, 11 Nov 2017 22:59:06 +0000

libhtml-formhandler-perl (0.40067-1) unstable; urgency=medium

  * Import upstream version 0.40067
  * debian/copyright: Add upstream email.
  * debian/control: Add libjson-maybexs-perl to {Build}-Depends.

 -- Angel Abad <angel@debian.org>  Fri, 21 Oct 2016 09:52:54 +0200

libhtml-formhandler-perl (0.40066-1) unstable; urgency=medium

  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Import upstream version 0.40066.
  * Update years of packaging copyright.
  * autopkgtest: activate more smoke tests.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Sep 2016 19:45:36 +0200

libhtml-formhandler-perl (0.40065-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Angel Abad ]
  * Imported Upstream version 0.40065
  * Declare compliance with Debian policy 3.9.8
  * debian/copyright: Update copyright years

 -- Angel Abad <angel@debian.org>  Fri, 29 Apr 2016 10:18:33 +0200

libhtml-formhandler-perl (0.40064-1) unstable; urgency=medium

  * Add debian/upstream/metadata.
  * Import upstream version 0.40064.
  * Update years of packaging copyright.
  * Update (build) dependencies.
  * Mark package as autopkgtest-able.
  * Add optional modules to Recommends. Thanks to autopkgtest.
  * Bump debhelper compatibility level to 9.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Oct 2015 16:36:57 +0200

libhtml-formhandler-perl (0.40057-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.
  * Add notice about removed options to debian/NEWS.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Aug 2014 20:34:56 -0700

libhtml-formhandler-perl (0.40056-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream releases 0.40053, 0.40054.
  * Add (build) dependency on liblist-allutils-perl.
  * Refresh patch (offset).
  * Declare compliance with Debian Policy 3.9.5.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 0.40055
  * Drop 735051-fix-FTBFS.patch (applied upstream)
  * Drop pod-encoding.patch patch (applied upstream)

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * New upstream release 0.40056.
  * Add build-dependency on libtype-tiny-perl. Used in a test.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Aug 2014 11:38:38 -0700

libhtml-formhandler-perl (0.40050-2) unstable; urgency=medium

  * Team upload.
  * Add 735051-fix-FTBFS.patch.
    Fix test case which was dependent on a 2008+5 yts time span.
    (Closes: #735051)

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 30 Jan 2014 22:58:32 +0100

libhtml-formhandler-perl (0.40050-1) unstable; urgency=low

  * New upstream release.
  * Add a warning about a removed option to debian/NEWS.
  * debian/control: debhelper (>= 8). Newer versions don't help for
    arch:all packages, and debian/compat was still at 8 anyway.
  * Add patch fix POD encoding.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Oct 2013 22:25:13 +0200

libhtml-formhandler-perl (0.40027-1) unstable; urgency=low

  * Imported Upstream version 0.40027
  * debian/control: Build-Depends on debhelper (>= 9.20120312)

 -- Angel Abad <angel@debian.org>  Sun, 18 Aug 2013 16:50:56 +0200

libhtml-formhandler-perl (0.40026-1) unstable; urgency=low

  * Imported Upstream version 0.40026
  * debian/control: B-D-I on libpadwalker-perl

 -- Angel Abad <angel@debian.org>  Fri, 12 Jul 2013 12:15:37 +0200

libhtml-formhandler-perl (0.40025-1) unstable; urgency=low

  * Imported Upstream version 0.40025
  * debian/copyright: Update debian years

 -- Angel Abad <angel@debian.org>  Thu, 06 Jun 2013 19:54:09 +0200

libhtml-formhandler-perl (0.40022-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Mar 2013 13:58:32 +0100

libhtml-formhandler-perl (0.40020-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update years of upstream and packaging copyright.
  * Add (build) dependency on libjson-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Feb 2013 18:19:50 +0100

libhtml-formhandler-perl (0.40017-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Dec 2012 16:48:59 +0100

libhtml-formhandler-perl (0.40016-1) unstable; urgency=low

  * new upstream release
  * control: added /me to uploaders

 -- Dominique Dumont <dod@debian.org>  Tue, 16 Oct 2012 19:38:10 +0200

libhtml-formhandler-perl (0.40013-1) unstable; urgency=low

  * Imported Upstream version 0.40013
  * Add myself to Uploaders

 -- Angel Abad <angel@debian.org>  Tue, 26 Jun 2012 17:51:43 +0200

libhtml-formhandler-perl (0.40012-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Jun 2012 15:55:47 +0200

libhtml-formhandler-perl (0.40011-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 08 Jun 2012 21:31:59 +0200

libhtml-formhandler-perl (0.40010-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 21:30:07 +0200

libhtml-formhandler-perl (0.40007-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 May 2012 18:48:42 +0200

libhtml-formhandler-perl (0.40006-1) unstable; urgency=low

  * New upstream release.
  * Drop pod.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 13 Apr 2012 16:41:06 +0200

libhtml-formhandler-perl (0.40005-1) unstable; urgency=low

  * Initial release (closes: #580216).

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Apr 2012 23:31:17 +0200
